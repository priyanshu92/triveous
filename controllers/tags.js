const Bookmark = require("../models/bookmarks");
const Tags = require("../models/tags");
const { validationResult } = require("express-validator");
exports.addTag = async (req, res, next) => {
  const title = req.body.title;
  const tags = new Tags({
    title: title,
  });
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      message: "error",
      errors: errors.array(),
    });
  }
  tags
    .save()
    .then((result) => {
      console.log(result);
      res.status(201).json({
        message: "Tag Created Succesfully",
        tag: result,
      });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getAllTags = async (req, res, next) => {
  Tags.find({})
    .then((tags) => {
      res.send(tags);
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.delTag = (req, res, next) => {
  const tagId = req.params.tagId;
  Tags.findById(tagId)
    .then((tag) => {
      if (!tag) {
        const error = new Error("Could not find bookmark");
        error.statusCode = 404;
        throw error;
      }
      return Tags.findByIdAndDelete(tagId);
    })
    .then((result) => {
      res.status(200).json({ message: "Tag Deleted", tag: result });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
