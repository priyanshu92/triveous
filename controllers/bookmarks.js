const Bookmark = require("../models/bookmarks");
const Tags = require("../models/tags");
const { validationResult } = require("express-validator");
const tags = require("../models/tags");

exports.addBookmark = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      message: "error",
      errors: errors.array(),
    });
  }
  const link = req.body.link;
  const title = req.body.title;
  const publisher = req.body.publisher;
  const tags = req.body.tags;
  const bookmark = new Bookmark({
    link: link,
    title: title,
    publisher: publisher,
    tags: tags,
  });
  bookmark
    .save()
    .then((result) => {
      console.log(result);
      res.status(201).json({
        message: "Bookmark Created Succesfully",
        bookmark: result,
      });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.getAllBookmarks = async (req, res, next) => {
  Bookmark.find({})
    .then((bookmarks) => {
      res.send(bookmarks);
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.delBookmark = (req, res, next) => {
  const bookmarkId = req.params.bookmarkId;
  Bookmark.findById(bookmarkId)
    .then((bookmark) => {
      if (!bookmark) {
        const error = new Error("Could not find bookmark");
        error.statusCode = 404;
        throw error;
      }
      return Bookmark.findByIdAndDelete(bookmarkId);
    })
    .then((result) => {
      res.status(200).json({ message: "Bookmark Deleted", bookmark: result });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.addTag = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      message: "Invalid Id",
    });
  }
  const bookmarkId = req.body.bookmarkId;
  const tagId = req.body.tagId;
  Tags.findById(tagId)
    .then((tag) => {
      if (!tag) {
        res.status(400).json({ message: "Tag Not found" });
      } else {
        let flag = 1;
        Bookmark.findById(bookmarkId)
          .then((bookmark) => {
            bookmark.tags.forEach((tag) => {
              if (tag == tagId) {
                flag = 0;
                return res.status(400).json({ message: "Tag Already Exist" });
              }
            });
            if (flag) {
              Bookmark.findByIdAndUpdate(bookmarkId, {
                $push: { tags: tagId },
              })
                .then((result) => {
                  res.status(200).json({ message: "Tag Added" });
                })
                .catch((err) => {
                  if (!err.statusCode) {
                    err.statusCode = 500;
                  }
                  next(err);
                });
            }
          })
          .catch((err) => {
            if (!err.statusCode) {
              err.statusCode = 500;
            }
            next(err);
          });
      }
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.delTag = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      message: "Invalid Id",
    });
  }
  const bookmarkId = req.body.bookmarkId;
  const tagId = req.body.tagId;
  Tags.findById(tagId)
    .then((tag) => {
      if (!tag) {
        res.status(400).json({ message: "Tag Not found" });
      } else {
        let flag = 1;
        Bookmark.findById(bookmarkId)
          .then((bookmark) => {
            bookmark.tags.forEach((tag) => {
              if (tag == tagId) {
                flag = 0;
                Bookmark.findByIdAndUpdate(bookmarkId, {
                  $pull: { tags: tagId },
                })
                  .then((result) => {
                    res.status(200).json({ message: "Tag Deleted" });
                  })
                  .catch((err) => {
                    if (!err.statusCode) {
                      err.statusCode = 500;
                    }
                    next(err);
                  });
              }
            });
            if (flag) {
              res.status(400).json({
                message:
                  "The tag is not associated with the bookmark or already deleted",
              });
            }
          })
          .catch((err) => {
            if (!err.statusCode) {
              err.statusCode = 500;
            }
            next(err);
          });
      }
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
