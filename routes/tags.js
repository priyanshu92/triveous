const express = require("express");
const expressvalidator = require("express-validator");

const bookmark = require("../controllers/bookmarks");
const tag = require("../controllers/tags");

const Tag = require("../models/tags");

const router = express.Router();

router.post(
  "/addTag",
  [
    expressvalidator
      .body("title")
      .trim()
      .custom((value, { req }) => {
        return Tag.findOne({ title: value }).then((t) => {
          if (t) {
            return Promise.reject("Tag already exists");
          }
        });
      }),
  ],
  tag.addTag
);

router.get("/getAllTags", tag.getAllTags);

router.delete("/delTag/:tagId", tag.delTag);

module.exports = router;
