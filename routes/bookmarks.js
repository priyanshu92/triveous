const express = require("express");
const expressvalidator = require("express-validator");

const bookmark = require("../controllers/bookmarks");
const tag = require("../controllers/tags");

const Bookmark = require("../models/bookmarks");

const router = express.Router();

router.post(
  "/createbookmark",
  [
    expressvalidator
      .body("link")
      .trim()
      .isURL()
      .withMessage("invalid Url")
      .custom((value, { req }) => {
        return Bookmark.findOne({ link: value }).then((bm) => {
          if (bm) {
            return Promise.reject("Bookmark already exists");
          }
        });
      }),
  ],
  bookmark.addBookmark
);

router.get("/getbookmarks", bookmark.getAllBookmarks);

router.post(
  "/addTag",
  [
    expressvalidator
      .body("bookmarkId")
      .trim()
      .isMongoId()
      .withMessage("Invalid Mongo ID"),
    expressvalidator
      .body("tagId")
      .trim()
      .isMongoId()
      .withMessage("Invalid Mongo ID"),
  ],
  bookmark.addTag
);

router.post(
  "/delTag",
  [
    expressvalidator
      .body("bookmarkId")
      .trim()
      .isMongoId()
      .withMessage("Invalid Mongo ID"),
    expressvalidator
      .body("tagId")
      .trim()
      .isMongoId()
      .withMessage("Invalid Mongo ID"),
  ],
  bookmark.delTag
);

router.delete("/deletebookmark/:bookmarkId", bookmark.delBookmark);

module.exports = router;
